<?php

declare(strict_types=1);

namespace Database;

use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Configuration as ORMConfiguration;
use Doctrine\DBAL\Connection;
use Database\Doctrine\ORM\EntityManagerFactory;
use Database\Doctrine\ORM\EntityManagerFactoryInterface;
use Database\Doctrine\DBAL\ConnectionFactory;
use Database\Doctrine\DBAL\ConnectionFactoryInterface;
use Database\Doctrine\ORM\ConfigurationFactory as ORMConfigurationFactory;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
    * Returns the container dependencies
    */
    public function getDependencies() : array
    {
        return [
            'alias' => [
                EntityManagerFactoryInterface::class => EntityManagerFactory::class,
                ConnectionFactoryInterface::class    => ConnectionFactory::class,
            ],
            'invokables' => [
                ConnectionFactory::class    => ConnectionFactory::class,
                EventManager::class => EventManager::class,
            ],
            'factories'  => [
                EntityManager::class => EntityManagerFactory::class,
                ORMConfiguration::class                => ORMConfigurationFactory::class,
                Connection::class                      => ConnectionFactory::class,
            ],
        ];
    }
}