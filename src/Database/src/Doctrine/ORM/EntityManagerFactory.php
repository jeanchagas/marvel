<?php

namespace Database\Doctrine\ORM;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

/**
 * Class EntityManagerFactory
 *
 * @package Database\Doctrine\ORM
 */
final class EntityManagerFactory implements EntityManagerFactoryInterface
{
    /**
     * @param ContainerInterface $container
     *
     * @throws \Doctrine\ORM\ORMException
     *
     * @return EntityManager
     */
    public function __invoke(ContainerInterface $container): EntityManager
    {
        $connection    = $container->get(Connection::class);
        $configuration = $container->get(Configuration::class);

        return $this->createEntityManager($connection, $configuration);
    }

    /**
     * @param Connection $connection
     *
     * @throws \Doctrine\ORM\ORMException
     *
     * @return EntityManager
     */
    public function createEntityManager(Connection $connection, Configuration $configuration): EntityManager
    {
        return EntityManager::create(
            $connection,
            $configuration
        );
    }
}