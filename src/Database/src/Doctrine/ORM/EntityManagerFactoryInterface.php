<?php

declare(strict_types=1);

namespace Database\Doctrine\ORM;


use Doctrine\DBAL\Connection;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;

/**
 * Interface EntityManagerFactoryInterface
 *
 * @package Database\Doctrine\ORM
 */
interface EntityManagerFactoryInterface
{
    /**
     * @param \Doctrine\DBAL\Connection $connection
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function createEntityManager(Connection $connection, Configuration $configuration): EntityManager;
}