<?php

declare(strict_types=1);

namespace Database\Doctrine\ORM;

use Doctrine\ORM\Configuration;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;

/**
 * Class ConfigurationFactory
 *
 * @package Database\Doctrine\ORM
 */
final class ConfigurationFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return Configuration
     */
    public function __invoke(ContainerInterface $container): Configuration
    {
        $config = $container->get('config');

        $configuration = Setup::createAnnotationMetadataConfiguration(
            $config[self::class]['entity_paths'],
            $config[self::class]['isDevMode']
        );

        return $configuration;
    }
}
