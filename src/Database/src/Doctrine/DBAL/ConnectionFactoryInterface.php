<?php

declare(strict_types=1);

namespace Database\Doctrine\DBAL;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;

/**
 * Interface ConnectionFactoryInterface
 *
 * @package Database\Doctrine\DBAL
 */
interface ConnectionFactoryInterface
{
    /**
     * @param array             $config
     * @param null|EventManager $eventManager
     *
     * @return Connection
     */
    public function createConnection(array $config, ?EventManager $eventManager = null): Connection;
}
