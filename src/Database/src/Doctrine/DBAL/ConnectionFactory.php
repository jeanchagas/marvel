<?php

declare(strict_types=1);
/**
 * @package Database\Doctrine\DBAL
 */
namespace Database\Doctrine\DBAL;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Psr\Container\ContainerInterface;

/**
 * Class ConnectionFactory
 *
 * @package Database\Doctrine\DBAL
 */
class ConnectionFactory implements ConnectionFactoryInterface
{
    /** @var EventManager */
    private $eventManager;

    /**
     * @param ContainerInterface $container
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return Connection
     */
    public function __invoke(ContainerInterface $container): Connection
    {
        $config       = $container->get('config');
        $eventManager = $container->get(EventManager::class);

        return $this->createConnection($config[self::class], $eventManager);
    }

    /**
     * @param array             $config
     * @param null|EventManager $eventManager
     *
     * @throws \Doctrine\DBAL\DBALException
     *
     * @return Connection
     */
    public function createConnection(array $config, ?EventManager $eventManager = null): Connection
    {
        return DriverManager::getConnection(
            $config,
            null,
            $this->eventManager
        );
    }
}
