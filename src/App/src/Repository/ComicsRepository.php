<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ComicsEntity;
use App\Entity\StoriesEntity;

/**
 * Class ComicsRepository
 * @package App\Repository
 */
class ComicsRepository extends AbstractRepository
{
    /**
     * @param int $id
     * @return ComicsEntity|null
     */
    public function findById(int $id): ?ComicsEntity
    {
        /** @var ComicsEntity $comics */
        $comics = $this->objectRepository->find($id);

        if ($comics instanceof ComicsEntity) {
            $this->logger->debug('Carregado entidade Comics com sucesso', $comics->toArray());
        } else {
            $this->logger->debug('Entidade Comics informada não foi carregada, pois não foi encontrada');
        }

        return $comics;
    }

    public function findAll() : array
    {
        /** @var array comics */
        $comics = $this->objectRepository->findAll();

        foreach ($comics as $comic) {
            if ($comic instanceof ComicsEntity) {
                $this->logger->debug('Carregado entidade Comics com sucesso', $comic->toArray());
            } else {
                $this->logger->debug('Entidade Comics informada não foi carregada, pois não foi encontrada');
            }
        }

        return $comics;
    }

    public function findBy(array $where): array
    {
        $comics = $this->objectRepository->findBy($where, ['id' => 'ASC'], 5);

        foreach ($comics as $comic) {
            if ($comic instanceof ComicsEntity) {
                $this->logger->debug('Carregado entidade Comics com sucesso', $comic->toArray());
            } else {
                $this->logger->debug('Entidade Comics informada não foi carregada, pois não foi encontrada');
            }
        }

        return $comics;
    }

    public function findByStory(StoriesEntity $story) : array
    {
        $query = $this->objectRepository->createQueryBuilder('c');
        $query->where('IDENTITY(c.story) = :story')
            ->setParameter('story', $story);

        return $query->getQuery()->getResult();
    }
}

