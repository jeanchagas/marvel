<?php

declare(strict_types=1);

namespace App\Repository\Factory;

use App\Repository\RepositoryInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\StoriesEntity;
use App\Repository\StoriesRepository;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class StoriesRepositoryFactory
 * @package App\Repository\Factory
 */
final class StoriesRepositoryFactory
{
    public function __invoke(ContainerInterface $container): RepositoryInterface
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager    = $container->get(EntityManager::class);
        $objectRepository = $entityManager->getRepository(StoriesEntity::class);
        $logger           = $container->get(Logger::class);

        return new StoriesRepository($objectRepository, $logger);
    }
}
