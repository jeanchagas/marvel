<?php

declare(strict_types=1);

namespace App\Repository\Factory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\CharactersEntity;
use App\Repository\CharactersRepository;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class CharactersRepositoryFactory
 *
 * @package \App\Repository\Factory
 */
final class CharactersRepositoryFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return CharactersRepository
     */
    public function __invoke(ContainerInterface $container): CharactersRepository
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager    = $container->get(EntityManager::class);
        $objectRepository = $entityManager->getRepository(CharactersEntity::class);
        $logger           = $container->get(Logger::class);

        return new CharactersRepository($objectRepository, $logger);
    }
}
