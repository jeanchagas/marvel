<?php

declare(strict_types=1);

namespace App\Repository\Factory;

use App\Repository\RepositoryInterface;
use App\Repository\ComicsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ComicsEntity;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

/**
 * Class StoriesRepositoryFactory
 * @package App\Repository\Factory
 */
final class ComicsRepositoryFactory
{
    public function __invoke(ContainerInterface $container): RepositoryInterface
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager    = $container->get(EntityManager::class);
        $objectRepository = $entityManager->getRepository(ComicsEntity::class);
        $logger           = $container->get(Logger::class);

        return new ComicsRepository($objectRepository, $logger);
    }
}
