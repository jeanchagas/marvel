<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CharactersEntity;
use App\Entity\StoriesEntity;

/**
 * Class StoriesRepository
 * @package App\Repository
 */
final class StoriesRepository extends AbstractRepository
{
    /**
     * @param int $id
     * @return StoriesEntity|null
     */
    public function findById(int $id): ?StoriesEntity
    {
        /** @var StoriesEntity $stories */
        $stories = $this->objectRepository->find($id);

        if ($stories instanceof StoriesEntity) {
            $this->logger->debug('Carregado entidade Stories com sucesso', $stories->toArray());
        } else {
            $this->logger->debug('Entidade Stories informada não foi carregada, pois não foi encontrada');
        }

        return $stories;
    }

    public function findAll() : array
    {
        /** @var array $stories */
        $stories = $this->objectRepository->findAll();

        foreach ($stories as $story) {
            if ($story instanceof StoriesEntity) {
                $this->logger->debug('Carregado entidade Stories com sucesso', $story->toArray());
            } else {
                $this->logger->debug('Entidade Stories informada não foi carregada, pois não foi encontrada');
            }
        }

        return $stories;
    }

    /**
     * @param CharactersEntity $character
     * @return array
     */
    public function findByCharacter(CharactersEntity $character) : array
    {
        $query = $this->objectRepository->createQueryBuilder('s');
        $query->where('IDENTITY(s.character) = :character')
            ->setParameter('character', $character);

        return $query->getQuery()->getResult();
    }
}
