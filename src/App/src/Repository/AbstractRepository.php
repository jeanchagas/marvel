<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;

abstract class AbstractRepository implements RepositoryInterface
{
    /** @var EntityRepository */
    protected $objectRepository;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * AbstractRepository constructor.
     *
     * @param ObjectRepository                $objectRepository
     * @param LoggerInterface                 $logger
     */
    public function __construct(
        ObjectRepository $objectRepository,
        LoggerInterface $logger
    ) {
        $this->objectRepository = $objectRepository;
        $this->logger           = $logger;
    }
}
