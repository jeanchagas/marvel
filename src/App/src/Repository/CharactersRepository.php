<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CharactersEntity;

/**
 * Class CharasctersEntity
 *
 * @package App\Repository
 */
final class CharactersRepository extends AbstractRepository
{
    /**
     * @param int $id
     *
     * @return null|CharactersEntity
     */
    public function findById(int $id): ?CharactersEntity
    {
        /** @var CharactersEntity $character */
        $character = $this->objectRepository->find($id);

        if ($character instanceof CharactersEntity) {
            $this->logger->debug('Carregado entidade Characters com sucesso', $character->toArray());
        } else {
            $this->logger->debug('Entidade Charascters informada não foi carregada, pois não foi encontrada');
        }

        return $character;
    }

    /**
     * @return array
     */
    public function findAll() : array
    {
        /** @var array $charascters */
        $characters = $this->objectRepository->findAll();

        foreach ($characters as $character) {
            if ($character instanceof CharactersEntity) {
                $this->logger->debug('Carregado entidade Characters com sucesso', $character->toArray());
            } else {
                $this->logger->debug('Entidade Charascters informada não foi carregada, pois não foi encontrada');
            }
        }

        return $characters;
    }
}
