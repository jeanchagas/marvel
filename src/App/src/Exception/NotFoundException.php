<?php

declare(strict_types=1);

namespace App\Exception;

use Zend\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Zend\ProblemDetails\Exception\ProblemDetailsExceptionInterface;

/**
 * Class NotFoundException
 *
 * @package App\Exception
 */
class NotFoundException extends \RuntimeException implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    public static function create(string $message, int $code): self
    {
        $exception            = new self($message);
        $exception->status    = 404;
        $exception->detail    = $message;
        $exception->type      = 'Not Found Exception';
        $exception->title     = 'Not found Exception';
        $exception->code      = $code;

        return $exception;
    }
}
