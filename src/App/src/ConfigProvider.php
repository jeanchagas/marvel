<?php

declare(strict_types=1);

namespace App;

final class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies() : array
    {
        return [
            'invokables' => [

            ],
            'factories'  => [
                Handler\HomePageHandler::class => Handler\Factory\HomePageHandlerFactory::class,
                Handler\ComicsPageHandler::class => Handler\Factory\ComicsPageHandlerFactory::class,
                Handler\StoriesPageHandler::class => Handler\Factory\StoriesPageHandlerFactory::class,
                Handler\StoryPageHandler::class => Handler\Factory\StoryPageHandlerFactory::class,
                Handler\ComicPageHandler::class => Handler\Factory\ComicPageHandlerFactory::class,
                Action\SearchCharactersAction::class => Action\Factory\SearchCharactersActionFactory::class,
                Action\SearchStoriesAction::class => Action\Factory\SearchStoriesActionFactory::class,
                Action\SearchComicsAction::class => Action\Factory\SearchComicsActionFactory::class,
                Action\SearchAllDataAction::class => Action\Factory\SearchAllDataActionFactory::class,
                Persister\CharactersPersister::class => Persister\Factory\CharactersPersisterFactory::class,
                Persister\StoriesPersister::class => Persister\Factory\StoriesPersisterFactory::class,
                Persister\ComicsPersister::class => Persister\Factory\ComicsPersisterFactory::class,
                Repository\CharactersRepository::class => Repository\Factory\CharactersRepositoryFactory::class,
                Repository\StoriesRepository::class => Repository\Factory\StoriesRepositoryFactory::class,
                Repository\ComicsRepository::class => Repository\Factory\ComicsRepositoryFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'app'    => [__DIR__ . '/../templates/app'],
                'error'  => [__DIR__ . '/../templates/error'],
                'layout' => [__DIR__ . '/../templates/layout'],
            ],
        ];
    }
}
