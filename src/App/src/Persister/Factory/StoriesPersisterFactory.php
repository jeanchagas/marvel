<?php

declare(strict_types=1);

namespace App\Persister\Factory;

use App\Persister\StoriesPersister;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class StoriesPersisterFactory
 *
 * @package App\Persister\Factory
 */
final class StoriesPersisterFactory
{
    /**
     * @param ContainerInterface $container
     * @return StoriesPersister
     */
    public function __invoke(ContainerInterface $container): StoriesPersister
    {
        $client         = $container->get(ClientInterface::class);
        $requestFactory = $container->get(RequestFactoryInterface::class);
        $streamFactory  = $container->get(StreamFactoryInterface::class);
        $logger         = $container->get(LoggerInterface::class);
        $entityManager  = $container->get(EntityManager::class);
        $config         = $container->get('config');

        return new StoriesPersister(
            $client,
            $requestFactory,
            $streamFactory,
            $logger,
            $entityManager,
            $config['marvel']
        );
    }
}