<?php

declare(strict_types=1);

namespace App\Persister\Factory;

use App\Persister\ComicsPersister;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ComicsPersisterFactory
 *
 * @package App\Persister\Factory
 */
final class ComicsPersisterFactory
{
    /**
     * @param ContainerInterface $container
     * @return ComicsPersister
     */
    public function __invoke(ContainerInterface $container): ComicsPersister
    {
        $client         = $container->get(ClientInterface::class);
        $requestFactory = $container->get(RequestFactoryInterface::class);
        $streamFactory  = $container->get(StreamFactoryInterface::class);
        $logger         = $container->get(LoggerInterface::class);
        $entityManager  = $container->get(EntityManager::class);
        $config         = $container->get('config');

        return new ComicsPersister(
            $client,
            $requestFactory,
            $streamFactory,
            $logger,
            $entityManager,
            $config['marvel']
        );
    }
}
