<?php

declare(strict_types=1);

namespace App\Persister;

use App\Entity\ComicsEntity;
use App\Entity\StoriesEntity;
use Doctrine\ORM\EntityManager;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;

final class ComicsPersister
{
    /** @var ClientInterface */
    private $client;

    /** @var RequestFactoryInterface */
    private $requestFactory;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    /** @var LoggerInterface */
    private $logger;

    /** @var EntityManager $entityManager */
    private $entityManager;

    /** @var array */
    private $config;

    /** @var string */
    private $url;

    /**
     * ComicsPersister constructor.
     * @param ClientInterface $client
     * @param RequestFactoryInterface $requestFactory
     * @param StreamFactoryInterface $streamFactory
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        LoggerInterface $logger,
        EntityManager $entityManager,
        array $config
    ) {
        $this->client         = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory  = $streamFactory;
        $this->logger         = $logger;
        $this->entityManager  = $entityManager;
        $this->config         = $config;

        $this->url = $this->config['protocol'] . '://' . $this->config['url'];
    }


    /**
     * @param string $operation
     * @return array
     * @throws ClientExceptionInterface
     */
    public function send(string $operation) : array
    {
        $request = $this->requestFactory->createRequest('GET', $this->getUri($operation))
            ->withHeader('Content-Type', 'application/json');

        $response = $this->client->sendRequest($request);

        $data =  \json_decode($response->getBody()->getContents(), true);

        return $data;
    }

    private function getUri(string $operation) : string
    {
        return "{$this->url}{$operation}?{$this->getQuery()}";
    }

    private function getQuery() : string
    {
        $query['ts'] = time();
        $query['apikey'] = $this->config['key']['public'];
        $query['hash'] = md5("{$query['ts']}{$this->config['key']['private']}{$query['apikey']}");

        return http_build_query($query);
    }

    /**
     * @param array $data
     * @param StoriesEntity $story
     * @return ComicsEntity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register(array $data, StoriesEntity $story) : ComicsEntity
    {
        $result = $data['data']['results'][0];

        $codeExternal = $result['id'];
        $title = $result['title'];
        $description = $result['description'];
        $thumbnail = $result['thumbnail']['path'].".".$result['thumbnail']['extension'];
        $image = $result['images'][0]['path'].".".$result['images'][0]['extension'];


        $comic = new ComicsEntity($title, $description, $thumbnail, $image, $codeExternal, $story);

        $this->entityManager->persist($comic);
        $this->entityManager->flush();

        $this->logger->info('Novo story inserido com sucesso', $comic->toArray());

        return $comic;
    }
}
