<?php

declare(strict_types=1);

namespace App\Persister;

use App\Entity\CharactersEntity;
use Doctrine\ORM\EntityManager;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Log\LoggerInterface;

/**
 * Class CharactersPersister
 *
 * @package App\Persister
 */
final class CharactersPersister
{
    /** @var ClientInterface */
    private $client;

    /** @var RequestFactoryInterface */
    private $requestFactory;

    /** @var StreamFactoryInterface */
    private $streamFactory;

    /** @var LoggerInterface */
    private $logger;

    /** @var EntityManager $entityManager */
    private $entityManager;

    /** @var array */
    private $config;

    /** @var string */
    private $url;

    /**
     * CharactersPersister constructor.
     * @param ClientInterface $client
     * @param RequestFactoryInterface $requestFactory
     * @param StreamFactoryInterface $streamFactory
     * @param LoggerInterface $logger
     * @param EntityManager $entityManager
     * @param array $config
     */
    public function __construct(
        ClientInterface $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface $streamFactory,
        LoggerInterface $logger,
        EntityManager $entityManager,
        array $config
    ) {
        $this->client         = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory  = $streamFactory;
        $this->logger         = $logger;
        $this->entityManager  = $entityManager;
        $this->config         = $config;

        $this->url = $this->config['protocol'] . '://' . $this->config['url'];
    }

    /**
     * @param string $operation
     * @return CharactersEntity
     * @throws ClientExceptionInterface
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function send(string $operation) : CharactersEntity
    {
        $request = $this->requestFactory->createRequest('GET', $this->getUri($operation))
            ->withHeader('Content-Type', 'application/json');

        $response = $this->client->sendRequest($request);

        $data =  \json_decode($response->getBody()->getContents(), true);

        return $this->register($data);
    }

    /**
     * @return string
     */
    private function getQuery() : string
    {
        $query['ts'] = time();
        $query['apikey'] = $this->config['key']['public'];
        $query['hash'] = md5("{$query['ts']}{$this->config['key']['private']}{$query['apikey']}");

        return http_build_query($query);
    }

    /**
     * @param string $operation
     * @return string
     */
    private function getUri(string $operation) : string
    {
        return "{$this->url}{$operation}?{$this->getQuery()}";
    }

    /**
     * @param array $data
     * @return CharactersEntity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function register(array $data) : CharactersEntity
    {
        $result = $data['data']['results'][0];

        $codeExternal = $result['id'];
        $name = $result['name'];
        $description = $result['description'];
        $image = $result['thumbnail']['path'].".".$result['thumbnail']['extension'];

        $character = new CharactersEntity($name, $description, $image, $codeExternal);

        $this->entityManager->persist($character);
        $this->entityManager->flush();

        $this->logger->info('Novo character inserido com sucesso', $character->toArray());

        return $character;
    }
}
