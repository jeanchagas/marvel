<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use App\Action\SearchActionInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

final class ComicsPageHandler implements RequestHandlerInterface
{

    private $config;

    /** @var SearchActionInterface */
    private $action;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        SearchActionInterface $action,
        TemplateRendererInterface $template,
        array $config
    ) {
        $this->action       = $action;
        $this->template     = $template;
        $this->config       = $config;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        try {
            $id = (int) $request->getAttribute('id');
            $data = array_merge(
                $this->config['application'],
                $this->action->search($id)
            );

            return new HtmlResponse($this->template->render('app::comics-page', $data));
        } catch (\Exception $e) {
            /**
             * @ TODO Tratar exceções
             */

            $data = $this->config['application'];
            return new HtmlResponse($this->template->render('error::404', $data));
        }
    }
}
