<?php

declare(strict_types=1);

namespace App\Handler;

use App\Action\SearchAllDataAction;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

final class HomePageHandler implements RequestHandlerInterface
{
    /** @var array */
    private $config;

    /** @var SearchAllDataAction */
    private $action;

    /** @var null|TemplateRendererInterface */
    private $template;

    public function __construct(
        SearchAllDataAction $action,
        TemplateRendererInterface $template,
        array $config
    ) {
        $this->action        = $action;
        $this->template      = $template;
        $this->config        = $config;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        try {
            $data = array_merge(
                $this->config['application'],
                $this->action->search()
            );

            return new HtmlResponse($this->template->render('app::home-page', $data));
        } catch (\Exception $e) {
            /**
             * @ TODO Tratar exceções
             */

            $data = $this->config['application'];
            return new HtmlResponse($this->template->render('error::404', $data));
        }
    }
}
