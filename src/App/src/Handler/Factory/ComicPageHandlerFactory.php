<?php

declare(strict_types=1);

namespace App\Handler\Factory;

use App\Action\SearchComicsAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Handler\ComicPageHandler;

final class ComicPageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $template = $container->get(TemplateRendererInterface::class);
        $action = $container->get(SearchComicsAction::class);
        $config = $container->get('config');

        return new ComicPageHandler($action, $template, $config);
    }
}
