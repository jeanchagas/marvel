<?php

declare(strict_types=1);

namespace App\Handler\Factory;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use App\Action\SearchAllDataAction;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Handler\HomePageHandler;

final class HomePageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $template = $container->get(TemplateRendererInterface::class);
        $action = $container->get(SearchAllDataAction::class);
        $config = $container->get('config');


        return new HomePageHandler($action, $template, $config);
    }
}
