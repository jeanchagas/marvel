<?php

declare(strict_types=1);

namespace App\Handler\Factory;

use App\Action\SearchAllDataAction;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use App\Handler\ComicsPageHandler;

final class ComicsPageHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $template = $container->get(TemplateRendererInterface::class);
        $action = $container->get(SearchAllDataAction::class);
        $config = $container->get('config');

        return new ComicsPageHandler($action, $template, $config);
    }
}
