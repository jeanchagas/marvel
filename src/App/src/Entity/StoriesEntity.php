<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\EntityTrait\Id;
use App\Entity\EntityTrait\Timestamps;

/**
 * @Entity
 * @Table(name="stories")
 **/
class StoriesEntity implements EntityInterface
{
    use Id,
        Timestamps;

    /**
     * @Column(name="title", type="string", length=100)
     *
     * @var string
     */
    protected $title;

    /**
     * @Column(name="description", type="string", length=300)
     *
     * @var string
     */
    protected $description;

    /**
     * @Column(name="code_external", type="integer")
     *
     * @var int
     */
    protected $codeExternal;

    /**
     * @ManyToOne(targetEntity="CharactersEntity")
     * @JoinColumn(name="character_id", referencedColumnName="id")
     *
     * @var CharactersEntity
     */
    protected $character;


    public function __construct(
        string $title,
        string $description = null,
        int $codeExternal = null,
        CharactersEntity $character = null
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->codeExternal = $codeExternal;
        $this->character = $character;
    }

    /**
     * @return int
     */
    public function getCodeExternal(): int
    {
        return $this->codeExternal;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return \get_object_vars($this);
    }
}
