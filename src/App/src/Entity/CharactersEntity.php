<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\EntityTrait\Id;
use App\Entity\EntityTrait\Timestamps;

/**
 * @Entity
 * @Table(name="characters")
 **/
class CharactersEntity implements EntityInterface
{
    use Id,
        Timestamps;

    /**
     * @Column(name="name", type="string", length=100)$this->config['character']['captain-america']['id']
     *
     * @var string
     */
    protected $name;

    /**
     * @Column(name="description", type="string", length=300)
     *
     * @var string
     */
    protected $description;

    /**
     * @Column(name="image", type="string", length=100)
     *
     * @var string
     */
    protected $image;


    /**
     * @Column(name="code_external", type="integer")
     *
     * @var int
     */
    protected $codeExternal;

    /**
     * CharactersEntity constructor.
     * @param string $name
     * @param string|null $description
     * @param string|null $image
     * @param int|null $codeExternal
     */
    public function __construct(
        string $name,
        string $description = null,
        string $image = null,
        int $codeExternal = null
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->codeExternal = $codeExternal;
    }

    /**
     * @return int
     */
    public function getCodeExternal(): int
    {
        return $this->codeExternal;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return \get_object_vars($this);
    }
}
