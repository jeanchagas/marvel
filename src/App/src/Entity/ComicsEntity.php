<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\EntityTrait\Id;
use App\Entity\EntityTrait\Timestamps;

/**
 * @Entity
 * @Table(name="comics")
 **/
class ComicsEntity implements EntityInterface
{
    use Id,
        Timestamps;

    /**
     * @Column(name="title", type="string", length=100)
     *
     * @var string
     */
    protected $title;

    /**
     * @Column(name="description", type="string", length=300)
     *
     * @var string
     */
    protected $description;

    /**
     * @Column(name="thumbnail", type="string", length=100)
     *
     * @var string
     */
    protected $thumbnail;

    /**
     * @Column(name="image", type="string", length=100)
     *
     * @var string
     */
    protected $image;

    /**
     * @ManyToOne(targetEntity="StoriesEntity")
     * @JoinColumn(name="story_id", referencedColumnName="id")
     *
     * @var StoriesEntity
     */
    protected $story;

    /**
     * @Column(name="code_external", type="integer")
     *
     * @var int
     */
    protected $codeExternal;

    /**
     * ComicsEntity constructor.
     * @param string $title
     * @param string $description
     * @param string $thumbnail
     * @param string $image
     * @param int $codeExternal
     * @param int $storyId
     */
    public function __construct(
        string $title,
        string $description = null,
        string $thumbnail = null,
        string $image = null,
        int $codeExternal = null,
        StoriesEntity $story = null
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->thumbnail = $thumbnail;
        $this->image = $image;
        $this->codeExternal = $codeExternal;
        $this->story = $story;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return \get_object_vars($this);
    }
}
