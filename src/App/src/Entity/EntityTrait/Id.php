<?php

declare(strict_types=1);

namespace App\Entity\EntityTrait;

/**
 * Trait Id
 *
 * @package App\Entity\EntityTrait
 */
trait Id
{
    /**
     * @Id @GeneratedValue
     * @Column(name="id", type="integer")
     *
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return  $this->id;
    }
}
