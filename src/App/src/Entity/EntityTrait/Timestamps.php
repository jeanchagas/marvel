<?php

declare(strict_types=1);

namespace App\Entity\EntityTrait;

/**
 * Trait Timestamps
 *
 * @package App\Entity\EntityTrait
 */
trait Timestamps
{
    /**
     * @Column(name="created", columnDefinition="TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
     *
     * @var \Datetime
     */
    protected $created;

    /**
     * @Column(name="updated", columnDefinition="TIMESTAMP NULL on update CURRENT_TIMESTAMP")
     *
     * @var \Datetime
     */
    protected $updated;
}
