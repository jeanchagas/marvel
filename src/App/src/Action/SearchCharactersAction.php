<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\CharactersEntity;
use App\Repository\CharactersRepository;
use App\Persister\CharactersPersister;
use App\Exception\NotFoundException;

final class SearchCharactersAction implements SearchActionInterface
{
    /** @var CharactersPersister */
    private $persister;

    /** @var CharactersRepository */
    private $repository;

    /** @var array */
    private $config;

    public function __construct(CharactersPersister $persister, CharactersRepository $repository, array $config)
    {
        $this->persister = $persister;
        $this->repository = $repository;
        $this->config = $config;
    }

    /**
     * @param int|null $id
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function search(int $id = null) : array
    {
        if ($id) {
            return $this->getOne($id);
        }

        return $this->getAll();
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getAll() : array
    {
        $characters = $this->repository->findAll();

        if (count($characters) === 0) {
            //throw NotFoundException::create('No charascters was found (NFD0005)', 5);
            $characters[0] = $this->getData($this->config['character']['captain-america']['id']);
            $characters[1] = $this->getData($this->config['character']['iron-man']['id']);
            $characters[2] = $this->getData($this->config['character']['spider-man']['id']);
        }

        return $characters;
    }

    /**
     * @param int $id
     * @return array
     */
    private function getOne(int $id) : array
    {
        $characters =$this->repository->findById($id);

        if (!$characters instanceof CharactersEntity) {
            throw NotFoundException::create("Characters not found with id: #{$id} (NFD0003)", 3);
        }

        $data[] = $characters;

        return $data;
    }

    /**
     * @param int $param
     * @return CharactersEntity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getData(int $param) : CharactersEntity
    {
        return $this->persister->send('characters/'.$param);
    }
}
