<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\ComicsEntity;
use App\Entity\StoriesEntity;
use App\Repository\StoriesRepository;
use App\Repository\ComicsRepository;
use App\Persister\ComicsPersister;
use App\Exception\NotFoundException;

/**
 * Class SearchComicsAction
 * @package App\Action
 */
final class SearchComicsAction implements SearchActionInterface
{
    /** @var ComicsPersister */
    private $persister;

    /** @var ComicsRepository */
    private $repository;

    /** @var StoriesRepository */
    private $storiesRepository;

    /** @var array */
    private $config;

    /**
     * SearchComicsAction constructor.
     * @param ComicsPersister $persister
     * @param ComicsRepository $repository
     * @param StoriesRepository $storiesRepository
     * @param array $config
     */
    public function __construct(
        ComicsPersister $persister,
        ComicsRepository $repository,
        StoriesRepository $storiesRepository,
        array $config
    ) {
        $this->persister = $persister;
        $this->repository = $repository;
        $this->storiesRepository = $storiesRepository;
        $this->config = $config;
    }

    /**
     * @param int|null $storiesId
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function search(int $storiesId = null): array
    {
        if ($storiesId) {
            return $this->getAllForStoriesId($storiesId);
        }

        return $this->getAll();
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getAll() : array
    {
        $comics = $this->repository->findAll();

        if (count($comics) === 0) {
            $stories = $this->storiesRepository->findAll();
            foreach ($stories as $story) {
                $comics [] = $this->getData($story);
            }
        }

        return $comics;
    }

    /**
     * @param int $id
     * @return array
     */
    public function getOne(int $id) : array
    {
        $comics = $this->repository->findById($id);

        if (!$comics instanceof ComicsEntity) {
            throw NotFoundException::create("Comics not found with id: #{$id} (NFD0003)", 3);
        }
        $data[] = $comics;

        return $data;
    }

    /**
     * @param StoriesEntity $story
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getData(StoriesEntity $story): array
    {
        $data = $this->persister->send('stories/'.$story->getCodeExternal().'/comics');
        $data = $this->persister->send('comics/'.$data['data']['results'][0]['id']);
        $datas[] = $this->persister->register($data, $story);

        return $datas;
    }

    private function getAllForStoriesId(int $storiesId) : array
    {
        $story = $this->storiesRepository->findById($storiesId);
        $comics = $this->repository->findByStory($story);

        if (count($comics) === 0) {
            $comics = $this->getData($story);
        }

        return $comics;
    }
}
