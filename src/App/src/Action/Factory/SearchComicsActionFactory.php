<?php

declare(strict_types=1);

namespace App\Action\Factory;

use App\Action\SearchActionInterface;
use App\Action\SearchComicsAction;
use App\Repository\ComicsRepository;
use App\Persister\ComicsPersister;
use App\Repository\StoriesRepository;
use Psr\Container\ContainerInterface;

/**
 * Class SearchComicsActionFactory
 * @package App\Action\Factory
 */
final class SearchComicsActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return SearchActionInterface
     */
    public function __invoke(ContainerInterface $container) : SearchActionInterface
    {
        $persister = $container->get(ComicsPersister::class);
        $repository = $container->get(ComicsRepository::class);
        $storiesRepository = $container->get(StoriesRepository::class);
        $config = $container->get('config');

        return new SearchComicsAction($persister, $repository, $storiesRepository, $config['marvel']);
    }
}
