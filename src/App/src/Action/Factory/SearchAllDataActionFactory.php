<?php

declare(strict_types=1);

namespace App\Action\Factory;

use App\Action\SearchActionInterface;
use App\Action\SearchAllDataAction;
use App\Action\SearchCharactersAction;
use App\Action\SearchStoriesAction;
use App\Action\SearchComicsAction;
use Psr\Container\ContainerInterface;

/**
 * Class SearchAllDataActionFactory
 * @package App\Action\Factory
 */
final class SearchAllDataActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return SearchActionInterface
     */
    public function __invoke(ContainerInterface $container) : SearchActionInterface
    {
        $charactersAction = $container->get(SearchCharactersAction::class);
        $storiesAction = $container->get(SearchStoriesAction::class);
        $comicsAction = $container->get(SearchComicsAction::class);

        $config = $container->get('config');

        return new SearchAllDataAction($charactersAction, $storiesAction, $comicsAction, $config['marvel']);
    }
}
