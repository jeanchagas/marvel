<?php

declare(strict_types=1);

namespace App\Action\Factory;

use App\Action\SearchActionInterface;
use App\Action\SearchCharactersAction;
use App\Repository\CharactersRepository;
use App\Persister\CharactersPersister;

use Psr\Container\ContainerInterface;

final class SearchCharactersActionFactory
{
    public function __invoke(ContainerInterface $container) : SearchActionInterface
    {
        $persister = $container->get(CharactersPersister::class);
        $repository = $container->get(CharactersRepository::class);
        $config = $container->get('config');

        return new SearchCharactersAction($persister, $repository, $config['marvel']);
    }
}
