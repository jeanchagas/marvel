<?php

declare(strict_types=1);

namespace App\Action\Factory;

use App\Action\SearchActionInterface;
use App\Action\SearchStoriesAction;
use App\Repository\CharactersRepository;
use App\Repository\StoriesRepository;
use App\Persister\StoriesPersister;
use Psr\Container\ContainerInterface;

/**
 * Class SearchStoriesActionFactory
 * @package App\Action\Factory
 */
final class SearchStoriesActionFactory
{
    /**
     * @param ContainerInterface $container
     * @return SearchActionInterface
     */
    public function __invoke(ContainerInterface $container) : SearchActionInterface
    {
        $persister = $container->get(StoriesPersister::class);
        $repository = $container->get(StoriesRepository::class);
        $charactersRepository = $container->get(CharactersRepository::class);
        $config = $container->get('config');

        return new SearchStoriesAction($persister, $repository, $charactersRepository, $config['marvel']);
    }
}
