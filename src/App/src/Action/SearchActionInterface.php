<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\EntityInterface;

interface SearchActionInterface
{
    public function search(int $id = null): array;
}
