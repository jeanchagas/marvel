<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\CharactersEntity;
use App\Entity\ComicsEntity;
use App\Entity\StoriesEntity;

final class SearchAllDataAction implements SearchActionInterface
{

    /** @var SearchActionInterface */
    private $charactersAction;

    /** @var SearchActionInterface */
    private $storiesAction;

    /** @var SearchActionInterface */
    private $comicsAction;

    /** @var array */
    private $config;

    /**
     * SearchAllDataAction constructor.
     * @param SearchActionInterface $charactersAction
     * @param SearchActionInterface $storiesAction
     * @param SearchActionInterface $comicsAction
     * @param array $config
     */
    public function __construct(
        SearchActionInterface $charactersAction,
        SearchActionInterface $storiesAction,
        SearchActionInterface $comicsAction,
        array $config
    ) {
        $this->config = $config;
        $this->charactersAction = $charactersAction;
        $this->storiesAction = $storiesAction;
        $this->comicsAction = $comicsAction;
    }

    /**
     * @param int|null $id
     * @return array
     */
    public function search(int $id = null): array
    {
        $characters = $this->charactersAction->search($id);
        $data = [];

        for ($i = 0; $i < 3; $i++) {
            if (!$characters[$i] instanceof CharactersEntity) {
                continue;
            }

            $data[$i]['character'] = $characters[$i]->toArray();
            $stories = $this->storiesAction->search($characters[$i]->getId());

            for ($j = 0; $j < 5; $j++) {
                $data[$i]['character']['stories'][$j] = $stories[$j]->toArray();
                $comics = $this->comicsAction->search($stories[$j]->getId());

                $data[$i]['character']['comics'][$j] = $comics[0]->toArray();
            }
        }
        $return['first'] = $data[0];
        $return['second'] = $data[1];
        $return['third'] = $data[2];

        return $return;
    }
}
