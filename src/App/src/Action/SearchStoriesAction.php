<?php

declare(strict_types=1);

namespace App\Action;

use App\Entity\StoriesEntity;
use App\Entity\CharactersEntity;
use App\Repository\CharactersRepository;
use App\Repository\StoriesRepository;
use App\Persister\StoriesPersister;
use App\Exception\NotFoundException;

/**
 * Class SearchStoriesAction
 * @package App\Action
 */
final class SearchStoriesAction implements SearchActionInterface
{
    /** @var StoriesPersister */
    private $persister;

    /** @var StoriesRepository */
    private $repository;

    /** @var StoriesRepository */
    private $charactersRepository;

    /** @var array */
    private $config;

    /**
     * SearchStoriesAction constructor.
     * @param StoriesPersister $persister
     * @param StoriesRepository $repository
     * @param CharactersRepository $charactersRepository
     * @param array $config
     */
    public function __construct(
        StoriesPersister $persister,
        StoriesRepository $repository,
        CharactersRepository $charactersRepository,
        array $config
    ) {
        $this->persister = $persister;
        $this->repository = $repository;
        $this->charactersRepository = $charactersRepository;
        $this->config = $config;
    }

    /**
     * @param int|null $characterId
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function search(int $characterId = null): array
    {
        if ($characterId) {
            return $this->getAllForCharacterId($characterId);
        }

        return $this->getAll();
    }

    /**
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getAll() : array
    {
        $stories = $this->repository->findAll();

        if (count($stories) === 0) {
            $characters = $this->charactersRepository->findAll();

            foreach ($characters as $character) {
                $stories [] = $this->getData($character);
            }
        }

        return $stories;
    }

    /**
     * @param Int $id
     * @return array
     */
    public function getOne(Int $id) : array
    {
        $stories = $this->repository->findById($id);

        if (!$stories instanceof StoriesEntity) {
            throw NotFoundException::create("Stories not found with id: #{$id} (NFD0003)", 3);
        }
        $data[] = $stories;
        return $data;
    }

    /**
     * @param CharactersEntity $character
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getData(CharactersEntity $character): array
    {
        $data =  $this->persister->send('characters/'.$character->getCodeExternal().'/stories');
        $datas = [];

        for ($i=0; $i < 5; $i++) {
            $story = $data['data']['results'][$i];
            $result = $this->persister->send('stories/'. $story['id']);
            $datas[] = $this->persister->register($result, $character);
        }

        return $datas;
    }

    /**
     * @param int $characterId
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    private function getAllForCharacterId(int $characterId) : array
    {
        $character = $this->charactersRepository->findById($characterId);
        $stories = $this->repository->findByCharacter($character);

        if (count($stories) === 0) {
            $stories = $this->getData($character);
        }

        return $stories;
    }
}
