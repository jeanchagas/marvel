<?php

declare(strict_types=1);

namespace Guzzle\Client\Factory;

use GuzzleHttp\Client;
use Guzzle\Client\GuzzlePSRClient;
use Psr\Container\ContainerInterface;

/**
 * Class GuzzlePSRClientFactory
 *
 * @package Guzzle\Client\Factory
 */
final class GuzzlePSRClientFactory
{
    /**
     * @param ContainerInterface $container
     *
     * @return GuzzlePSRClient
     */
    public function __invoke(ContainerInterface $container): GuzzlePSRClient
    {
        $guzzleClient = $container->get(Client::class);

        return new GuzzlePSRClient($guzzleClient);
    }
}
