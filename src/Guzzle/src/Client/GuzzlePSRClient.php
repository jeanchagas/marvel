<?php

declare(strict_types=1);

namespace Guzzle\Client;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\ConnectException as GuzzleConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use Guzzle\Exception\ClientException;
use Guzzle\Exception\ClientNetworkException;
use Guzzle\Exception\ClientRequestException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class GuzzlePSRClient
 *
 * @package Guzzle\Client
 */
final class GuzzlePSRClient implements ClientInterface
{
    /** @var GuzzleClient */
    private $guzzleClient;

    /**
     * GuzzlePSRClient constructor.
     *
     * @param GuzzleClient $guzzleClient
     */
    public function __construct(GuzzleClient $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
    }

    /**
     * @param RequestInterface $request
     *
     * @throws ClientException
     * @throws ClientNetworkException
     * @throws ClientRequestException
     *
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        try {
            return $this->guzzleClient->send($request);
        } catch (GuzzleClientException $guzzleClientException) {
            throw new ClientException(
                $request,
                'Error generated during client execution (CLT0001)',
                1,
                $guzzleClientException
            );
        } catch (GuzzleServerException $guzzleServerException) {
            throw new ClientException(
                $request,
                'Server error occurred during client execution (CLT0002)',
                2,
                $guzzleServerException
            );
        } catch (GuzzleConnectException $guzzleConnectionException) {
            throw new ClientNetworkException(
                $request,
                'Generated error while establishing client connection (CTN 0001)',
                1,
                $guzzleConnectionException
            );
        } catch (GuzzleRequestException $guzzleRequestException) {
            throw new ClientRequestException(
                $request,
                'Error generated in client request (CTR 0001)',
                1,
                $guzzleRequestException
            );
        } catch (\Throwable | GuzzleException $exception) {
            throw new ClientException(
                $request,
                'generated generic error on client (CTR 0003)',
                1,
                $exception
            );
        }
    }
}
