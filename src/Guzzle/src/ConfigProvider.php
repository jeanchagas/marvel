<?php

declare(strict_types=1);

namespace Guzzle;

use GuzzleHttp\Client;
use Guzzle\Client\Factory\GuzzlePSRClientFactory;
use Guzzle\Client\GuzzlePSRClient;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Zend\Diactoros\RequestFactory;
use Zend\Diactoros\StreamFactory;

/**
 * Class ConfigProvider
 *
 * @package Guzzle
 */
class ConfigProvider
{
    /**
     * Provide configs for the module
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Provides module dependencies
     */
    public function getDependencies(): array
    {
        return [
            'aliases' => [
                ClientInterface::class         => GuzzlePSRClient::class,
                RequestFactoryInterface::class => RequestFactory::class,
                StreamFactoryInterface::class  => StreamFactory::class
            ],
            'invokables' => [
                Client::class          => Client::class,
                RequestFactory::class  => RequestFactory::class,
                StreamFactory::class   => StreamFactory::class
            ],
            'factories' => [
                GuzzlePSRClient::class => GuzzlePSRClientFactory::class
            ]
        ];
    }
}
