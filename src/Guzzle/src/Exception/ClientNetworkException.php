<?php

declare(strict_types=1);

namespace Guzzle\Exception;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Class ClientNetworkException
 *
 * @package Guzzle\Exception
 */
final class ClientNetworkException extends AbstractPSRClientException implements ClientExceptionInterface
{
}
