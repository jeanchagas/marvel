<?php

declare(strict_types=1);

namespace Guzzle\Exception;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Class ClientException
 *
 * @package Guzzle\Exception
 */
final class ClientException extends AbstractPSRClientException implements ClientExceptionInterface
{
}
