<?php

declare(strict_types=1);

namespace Guzzle\Exception;

use Psr\Http\Message\RequestInterface;
use Throwable;

/**
 * Class AbstractPSRClientException
 *
 * @package Guzzle\Exception
 */
abstract class AbstractPSRClientException extends \Exception
{
    /** @var RequestInterface */
    protected $request;

    public function __construct(
        RequestInterface $request,
        string $message,
        $code,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);

        $this->request = $request;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }
}
