<?php

declare(strict_types=1);

namespace Guzzle\Exception;

use Psr\Http\Client\ClientExceptionInterface;

/**
 * Class ClientRequestException
 *
 * @package Guzzle\Exception
 */
final class ClientRequestException extends AbstractPSRClientException implements ClientExceptionInterface
{
}
