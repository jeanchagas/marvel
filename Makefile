MAKEFLAGS += --warn-undefined-variables
SHELL := sh
.DELETE_ON_ERROR:
.SUFFIXES:
.EXPORT_ALL_VARIABLES:
.ONESHELL:
.DEFAULT_GOAL := install

export UID=$(shell id -u)
export GID=$(shell id -g)

DOCKER_BIN := $(shell which docker)
COMPOSER_BIN := $(DOCKER_BIN) run --rm -it \
    				-v $(PWD):/app \
    				-v /tmp:/tmp \
    				-v /etc/passwd:/etc/passwd:ro \
    				-v /etc/group:/etc/group:ro \
    				-u $(UID):$(GID) \
    				composer

COMPOSER_FLAGS=--prefer-dist           \
                --optimize-autoloader

.PHONY: install clean reinstall

install: vendor .env composer.lock

reinstall: clean install

clean:
	rm -rf vendor composer.phar composer.lock

.env:
	cp -v .env.example .env

composer.phar:
	curl -s https://getcomposer.org/installer | php

vendor:
	$(COMPOSER_BIN) install $(COMPOSER_FLAGS)

composer.lock:
	$(COMPOSER_BIN) install $(COMPOSER_FLAGS)
