ARG PHP_VERSION=7.3
FROM composer:1.8 AS vendors

COPY ./composer.json /app
COPY ./composer.lock /app

RUN composer install --no-dev --ignore-platform-reqs

FROM php:${PHP_VERSION}-apache

ARG DEBUG="off"
ARG BUILD_TAG="latest"
ARG APP_DIR="/app"
ARG PUBLIC_DIR="public"
ARG TZ="America/Sao_Paulo"

ENV DEBUG="$DEBUG" \
    PHP_VERSION="${PHP_VERSION}" \
    BUILD_TAG="$BUILD_TAG" \
    APP_DIR="$APP_DIR" \
    APACHE_DOCUMENT_ROOT="${APP_DIR}/${PUBLIC_DIR}" \
    TZ="${TZ}"

RUN  set -eux; \
    sed -ri -e \
        's!/var/www/html!\$\{APACHE_DOCUMENT_ROOT\}!g' \
        ${APACHE_CONFDIR}/sites-available/*.conf \
    ; \
    sed -ri -e \
        's!/var/www/!\$\{APACHE_DOCUMENT_ROOT\}!g' \
        ${APACHE_CONFDIR}/apache2.conf \
        ${APACHE_CONFDIR}/conf-available/*.conf \
    ; \
    a2enmod rewrite

RUN docker-php-ext-install \
        pdo_mysql \
        mbstring \
        bcmath \
        json \
        opcache \
    ; \
    pecl install xdebug

RUN apt-get update && \
    apt-get install --yes --no-install-recommends \
        tzdata \
        git \
        zip \
    && \
    apt-get clean; \
    rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
    ;

RUN echo "${TZ}" > /etc/timezone \
    ln -vsfT /usr/share/zoneinfo/${TZ} /etc/localtime

COPY --from=vendors /usr/bin/composer /usr/bin/composer

WORKDIR ${APP_DIR}
USER ${APACHE_RUN_USER}

COPY --from=vendors /app/vendor ${APP_DIR}/vendor
COPY . ${APP_DIR}

RUN ln -vsfT ${APP_DIR}/bin/docker-php-entrypoint /usr/local/bin/docker-php-entrypoint
