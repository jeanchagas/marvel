![Marvel](public/resource/image/marvel.png)

## Config
To set up the application, you must generate Api Keys on the [developer marvel website](https://developer.marvel.com) via [Get Key](https://developer.marvel.com/account).

After that, you must enter the generated keys in the .env.example file replacing the value of the variables:

```bash
MARVEL_API_PUBLIC_KEY=public-key
MARVEL_API_PRIVATE_KEY=private-key
```

## Install

To install this application on a development environment just run:

```bash
$ make install
```

It will install the minimum files that the application needs to everything execute properly.

## Running

To provide a web server of this application run the following command:

```bash
$ docker-compose up -d
```
## Logs

It can be tracked through the container log:

```bash
$ docker logs -f marvel-web
```

## Container

After the libraries are installed, access the container created with the following command:

```bash
$ docker exec -it marvel-web bash
```

## Prerequisites

This implementation rely on the make, docker and docker-compose binaries.
Make sure that you have already installed this dependencies before execute the described commands of this file:

* [Docker](https://docs.docker.com/install/)
* [Docker Compose](https://docs.docker.com/compose/install/)
* [GNU Make](https://www.gnu.org/software/make/)

## Note

The first access, by loading large amount of data, adopts a loading of up to 20 seconds.