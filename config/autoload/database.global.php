<?php

declare(strict_types=1);

use Database\Doctrine\DBAL\ConnectionFactory;
use Database\Doctrine\Migration\ConfigurationFactory;
use Database\Doctrine\ORM\ConfigurationFactory as ORMConfiguration;

return [
    ConnectionFactory::class => [
        'dbname'        => \getenv('MYSQL_DATABASE'),
        'user'          => \getenv('MYSQL_USER'),
        'password'      => \getenv('MYSQL_PASSWORD'),
        'host'          => \getenv('MYSQL_HOST'),
        'driver'        => \getenv('MYSQL_DRIVER') ?: 'pdo_mysql',
        'charset'       => \getenv('MYSQL_CHARSET'),
        'driverOptions' => [
            1002 => 'SET NAMES utf8'
        ]
    ],
    ConfigurationFactory::class => [
        'name'                    => 'Migrations Contratos',
        'migrations_namespace'    => 'KingHost\Contrato\App\Database\Migration',
        'table_name'              => 'doctrine_migration_versions',
        'column_name'             => 'version',
        'column_length'           => 14,
        'executed_at_column_name' => 'executed_at',
        'migrations_directory'    => 'src/App/src/Database/Migration',
        'all_or_nothing'          => true,
    ],
    ORMConfiguration::class => [
        'isDevMode'    => \getenv('APP_ENV') === 'development',
        'entity_paths' => [
            'src/App/src/Entity',
            'src/KingHost/src/Entity'
        ],
    ],
];
