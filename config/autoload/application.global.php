<?php

declare(strict_types=1);

return [
    'application' => [
        'domain' => \getenv('HOSTNAME'),
        'logo' => 'resource/image/marvel.png',
        'favicon' => 'resource/image/favicon.ico',
    ],
];
