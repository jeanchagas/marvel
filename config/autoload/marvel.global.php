<?php

declare(strict_types=1);

return [
    'marvel' => [
        'protocol' =>'http',
        'url' => 'gateway.marvel.com/v1/public/',
        'key' =>[
            'public'  => \getenv('MARVEL_API_PUBLIC_KEY'),
            'private' => \getenv('MARVEL_API_PRIVATE_KEY'),
        ],
        'character' => [
            'spider-man' => [
                'id' => 1009610,
            ],
            'iron-man' => [
                'id' => 1009368,
            ],
            'captain-america' => [
                'id' => 1009220,
            ],
        ]
    ],
];
